package com.test.web.dao_user;

import com.test.web.entity.User;

import java.util.Map;

/**
 * Data Access Object interface
 * Created by ksu on 06.10.2016.
 */
public interface UserDao {

    /* New User Registration */
    void registerUser(User user);

    /* User Authentication */
    boolean loginUser(String login, String password);

    /* Get a list of all registered users */
    Map<Integer, User> getAllUsers();

    User getUser(String name);

}
