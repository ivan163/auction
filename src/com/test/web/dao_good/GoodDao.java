package com.test.web.dao_good;

import com.test.web.entity.Good;

import java.util.List;

/**
 * Created by ksu on 12.10.2016.
 */
public interface GoodDao {

    List<Good> getAllGoods();

    Good getGood(String good_name);
}
