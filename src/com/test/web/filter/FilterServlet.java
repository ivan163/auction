package com.test.web.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by ksu on 10.10.2016.
 */
public class FilterServlet implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String str = servletRequest.getParameter("bid");
        if(!"".equals(str)){
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
