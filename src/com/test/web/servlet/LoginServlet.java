package com.test.web.servlet;

import com.test.web.dao_user.UserDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Create class LoginServlet implementing interface HttpServlet
 * Class is used to enter the online auction, provided the correct login and password
 * Created by ksu on 07.10.2016.
 */
public class LoginServlet extends HttpServlet {

    UserDaoImpl userDao = new UserDaoImpl();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

//        //Displays links to other pages
//        req.getRequestDispatcher("link.html").include(req, resp);

        String name = req.getParameter("name");
        String password = req.getParameter("password");

        //The flag is used to determine whether a user database
        boolean flag = false;

        //Shows all current users
        System.out.println(userDao.getAllUsers());

        //If the database has the user, create session and set the attribute that is unique to this session
        if (userDao.loginUser(name, password)) {
            HttpSession session = req.getSession();
            session.setAttribute("name", name);
            req.getRequestDispatcher("AuctionServlet").include(req, resp);
            flag = true;
        }

        //If the database is not user, then show an error message
        if (!flag) {
            req.getRequestDispatcher("link.html").include(req, resp);
            req.getRequestDispatcher("alerts/alert2.html").include(req, resp);
            req.getRequestDispatcher("login.html").include(req, resp);
        }
        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
