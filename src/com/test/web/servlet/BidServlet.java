package com.test.web.servlet;

import com.test.web.dao_bid.BidDaoImpl;
import com.test.web.dao_good.GoodDaoImpl;
import com.test.web.dao_user.UserDaoImpl;
import com.test.web.entity.Bid;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by ksu on 15.10.2016.
 */
public class BidServlet extends HttpServlet {

    BidDaoImpl bidDao = new BidDaoImpl();

    UserDaoImpl userDao = new UserDaoImpl();

    GoodDaoImpl goodDao = new GoodDaoImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        //Displays links to other pages
        req.getRequestDispatcher("link.html").include(req, resp);

        Integer bid = Integer.valueOf(req.getParameter("bid"));
        String select_good = req.getParameter("select_good");

        //The flag is used to determine whether a user database
        boolean flag = false;

        if (bid != 0) {
            HttpSession session = req.getSession(false);
            String name = (String) session.getAttribute("name");
            bidDao.makeBid(new Bid(userDao.getUser(name), goodDao.getGood(select_good), bid));

            System.out.println(bidDao.getAllBid());
            flag = true;
        } else {
            req.getRequestDispatcher("alerts/alert2.html").include(req, resp);
            req.getRequestDispatcher("bid.html").include(req, resp);
        }

        if (flag) {
            req.getRequestDispatcher("/auction.jsp").include(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
