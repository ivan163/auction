package com.test.web.dao_bid;

import com.test.web.entity.Bid;
import com.test.web.entity.User;

import java.util.Map;

/**
 * Created by ksu on 12.10.2016.
 */
public class BidDaoImpl implements BidDao {
    static BidMap instance = BidMap.getInstance();


    @Override
    public void makeBid(Bid bid) {
        Bid bid1 = new Bid(bid.getUser(), bid.getGood(), bid.getBid());
        instance.setIntegerBidMap(bid1);
    }

    @Override
    public User determine_winner() {
        return null;
    }

    @Override
    public Map<Integer, Bid> getAllBid() {
        return instance.getIntegerBidMap();
    }
}
