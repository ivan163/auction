package com.test.web.servlet;

import com.test.web.dao_good.GoodDao;
import com.test.web.dao_good.GoodDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Create class AuctionServlet implementing interface HttpServlet
 * The class implements the interaction with the user in the online auction
 * Created by ksu on 07.10.2016.
 */
public class AuctionServlet extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        //Displays links to other pages
           req.getRequestDispatcher("link.html").include(req, resp);

        //Do not create a session if it already exists
        HttpSession session = req.getSession(false);
        if (session != null) {
            String name = (String) session.getAttribute("name");
            out.print("<h3 align=\"center\" style=\"color: black\">Welcome to Online Auction!</h3><br>");


            GoodDao goodDao = new GoodDaoImpl();
            System.out.println(goodDao.getAllGoods());


            session.setAttribute("goods_list", goodDao.getAllGoods());
//            RequestDispatcher rd = getServletContext().getRequestDispatcher("/auction.jsp");
//            rd.forward(req, resp);
            req.getRequestDispatcher("/auction.jsp").include(req, resp);

        } else {

            //If the session is not installed go to a page with a login and password
            req.getRequestDispatcher("alerts/alert4.html").include(req, resp);
            req.getRequestDispatcher("login.html").include(req, resp);
        }


        out.close();
    }
}

