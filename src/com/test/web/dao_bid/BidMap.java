package com.test.web.dao_bid;

import com.test.web.entity.Bid;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by ksu on 12.10.2016.
 */
public class BidMap{
    /**
     * Create an object of SingleObject
     */
    private static BidMap instance;

    /**
     * Map is working as database of users
     */
    private Map<Integer, Bid> integerBidMap = new ConcurrentHashMap<>();

    private static Integer count = 1;
    /**
     * Make the constructor private so that this class cannot be instantiated
     */
    private BidMap() {
    }

    /**
     * Get the only object available
     *
     * @return instance
     */
    public static BidMap getInstance() {
        if (instance == null) {
            instance = new BidMap();
        }
        return instance;
    }

    public Map<Integer, Bid> getIntegerBidMap() {
        return integerBidMap;
    }

    public void setIntegerBidMap(Bid bid) {
        this.integerBidMap.put(count,bid);
        count++;

    }

}
