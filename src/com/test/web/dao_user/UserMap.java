package com.test.web.dao_user;

import com.test.web.entity.User;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by ksu on 10.10.2016.
 */
public class UserMap {
    /**
     * Create an object of SingleObject
     */
    private static UserMap instance;

    /**
     * Map is working as database of users
     */
    private Map<Integer, User> users = new ConcurrentHashMap<>();

    /**
     * users numbering in a HashMap
     */
    private static Integer count = 1;

    /**
     * Make the constructor private so that this class cannot be instantiated
     */
    private UserMap() {
    }

    /**
     * Get the only object available
     *
     * @return instance
     */
    public static UserMap getInstance() {
        if (instance == null) {
            instance = new UserMap();
        }
        return instance;
    }

    public Map<Integer, User> getUsers() {
        return users;
    }

    public void setUsers(User user) {
        this.users.put(count, user);
        count++;
    }

    public User getUser(String name){
        for (User user : users.values()) {
            if(user.getLogin().equals(name)){
                return user;
            }
        }
        return null;
    }


}
