package com.test.web.dao_bid;

import com.test.web.entity.Bid;
import com.test.web.entity.User;

import java.util.Map;

/**
 * Created by ksu on 12.10.2016.
 */
public interface BidDao {
    /* The user makes a bid */
    void makeBid(Bid bid);

    /**
     * determine the user who made the biggest bet
     *
     * @return the user, who won the online auction
     */
    User determine_winner();

    Map<Integer, Bid> getAllBid();
}
