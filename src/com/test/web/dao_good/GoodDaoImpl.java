package com.test.web.dao_good;

import com.test.web.entity.Good;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ksu on 12.10.2016.
 */
public class GoodDaoImpl implements GoodDao {

    List<Good> goods;

    public GoodDaoImpl() {
        goods = new ArrayList<>();
        Good good1 = new Good("The Starry Night", "The Starry Night is an oil on canvas by the " +
                "Dutch post-impressionist painter Vincent van Gogh", 150);
        Good good2 = new Good("Portland Vase", "The Portland Vase is a Roman cameo glass vase, which is dated to between AD 1 and AD 25", 50);
        Good good3 = new Good("Portrait of an Unknown Woman", "Portrait of an Unknown Woman " +
                "is a painting by the Russian artist Ivan Kramskoi, drawn in 1883", 1000);
        Good good4 = new Good("The School of Athens", "The School of Athens is one of the most famous frescoes " +
                "by the Italian Renaissance artist Raphael", 650);
        goods.add(good1);
        goods.add(good2);
        goods.add(good3);
        goods.add(good4);
    }

    @Override
    public List<Good> getAllGoods() {
        return goods;
    }

    @Override
    public Good getGood(String good_name) {
        for (Good good : goods) {
            if (good.getName().equals(good_name)) {
                return good;
            }
        }
        return null;
    }

}
