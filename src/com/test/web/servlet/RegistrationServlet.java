package com.test.web.servlet;

import com.test.web.dao_user.UserDaoImpl;
import com.test.web.entity.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

/**
 * Create class RegistrationServlet implementing interface HttpServlet
 * The class is used to register new users
 * Created by ksu on 07.10.2016.
 */
public class RegistrationServlet extends HttpServlet {

    UserDaoImpl userDao = new UserDaoImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        //Displays links to other pages
        req.getRequestDispatcher("link.html").include(req, resp);

        String name = req.getParameter("name");
        String password = req.getParameter("password");

        //Create two users
        userDao.registerUser(new User("tom", "tom163"));
        userDao.registerUser(new User("bimo", "123"));

        //The flag is used to determine whether a user database
        boolean flag = false;

        //If the database is already a user with these login and password, we show an error message
        if (userDao.loginUser(name, password)) {
            req.getRequestDispatcher("alerts/alert1.html").include(req, resp);
            req.getRequestDispatcher("registration.html").include(req, resp);
            flag = true;
        }

        //If the database is not the user with these login and password,
        // as well as the username and password are not the empty string, then register a new user
        if (!flag && (!Objects.equals(name, "") || !Objects.equals(password, ""))) {
            userDao.registerUser(new User(name, password));
            req.getRequestDispatcher("alerts/alert3.html").include(req, resp);

            //Go to a page with a login and password to access the online auction
            req.getRequestDispatcher("login.html").include(req, resp);

            //Show all users
            System.out.println(userDao.getAllUsers());
        } else {

            //Otherwise, go back to the page with the registration
            if (!flag) {
                req.getRequestDispatcher("alerts/alert2.html").include(req, resp);
                req.getRequestDispatcher("registration.html").include(req, resp);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
