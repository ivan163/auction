package com.test.web.entity;

/**
 * Created by ksu on 06.10.2016.
 */
public class Bid {
    private User user;
    private Good good;
    private Integer bid;

    public Bid(User user, Good good, Integer bid) {
        this.user = user;
        this.good = good;
        this.bid = bid;
    }

    public Integer getBid() {
        return bid;
    }

    public void setBid(Integer bid) {
        this.bid = bid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Good getGood() {
        return good;
    }

    public void setGood(Good good) {
        this.good = good;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bid)) return false;

        Bid bid1 = (Bid) o;

        if (getUser() != null ? !getUser().equals(bid1.getUser()) : bid1.getUser() != null) return false;
        if (getGood() != null ? !getGood().equals(bid1.getGood()) : bid1.getGood() != null) return false;
        return getBid() != null ? getBid().equals(bid1.getBid()) : bid1.getBid() == null;

    }

    @Override
    public int hashCode() {
        int result = getUser() != null ? getUser().hashCode() : 0;
        result = 31 * result + (getGood() != null ? getGood().hashCode() : 0);
        result = 31 * result + (getBid() != null ? getBid().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Bid{" +
                "user=" + user +
                ", good=" + good +
                ", bid=" + bid +
                '}';
    }
}
