package com.test.web.entity;

/**
 * Created by ksu on 06.10.2016.
 */
public class Good {
    private String name;
    private String description;
    private Integer starting_price;

    public Good(String name, String description, Integer price) {

        this.name = name;
        this.description = description;
        this.starting_price = price;

    }

    public Integer getStarting_price() {
        return starting_price;
    }

    public void setStarting_price(Integer starting_price) {
        this.starting_price = starting_price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Good)) return false;

        Good good = (Good) o;

        if (getName() != null ? !getName().equals(good.getName()) : good.getName() != null) return false;
        if (getDescription() != null ? !getDescription().equals(good.getDescription()) : good.getDescription() != null)
            return false;
        return getStarting_price() != null ? getStarting_price().equals(good.getStarting_price()) : good.getStarting_price() == null;

    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getStarting_price() != null ? getStarting_price().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Good{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + starting_price +
                '}';
    }
}
