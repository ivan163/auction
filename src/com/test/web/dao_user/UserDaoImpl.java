package com.test.web.dao_user;

import com.test.web.entity.User;

import java.util.Map;

/**
 * Create class UserDaoImpl implementing interface UserDao
 * Created by ksu on 06.10.2016.
 */
public class UserDaoImpl implements UserDao {
    //комментируются все поля не private

    //Get the only object available
    static UserMap instance = UserMap.getInstance();

    public UserDaoImpl() {
    }

    /**
     * @param user New registered user is added to the HashMap
     * @see UserDao
     */
    @Override
    public void registerUser(User user) {
        if (!loginUser(user.getLogin(), user.getPassword())) {
            User user1 = new User(user.getLogin(), user.getPassword());
            instance.setUsers(user1);
        }
    }

    /**
     * @param login
     * @param password
     * @return true, if  if the entered login and password matches
     * the login to the database, returns false - otherwise
     */
    @Override
    public boolean loginUser(String login, String password) {
        for (Map.Entry<Integer, User> integerUserEntry : instance.getUsers().entrySet()) {
            if (login.equals(integerUserEntry.getValue().getLogin()) && password.equals(integerUserEntry.getValue().getPassword())) {
                return true;
            }
        }
        System.out.println("User don't found!");
        return false;
    }

    /**
     * @return Map of users
     */
    @Override
    public Map<Integer, User> getAllUsers() {
        return instance.getUsers();
    }

    @Override
    public User getUser(String name) {
        for (User user : instance.getUsers().values()) {
            if(user.getLogin().equals(name)){
                return user;
            }
        }
        return null;
    }
}
