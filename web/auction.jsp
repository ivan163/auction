<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ksu
  Date: 13.10.2016
  Time: 22:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Auction</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="table-responsive">
        <table class="table">
            <tr>
                <td>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Product Description</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${sessionScope.goods_list}" var="good">
                            <tr>
                                <td>
                                    <c:out value="${good.getName()}"/>
                                </td>
                                <td>
                                    <c:out value="${good.getDescription()}"/>
                                </td>
                                <td>
                                    <c:out value="${good.getStarting_price()}"/>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </td>
                <td>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Make Bid</th>
                            <th>More Information</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>
                                <a href="bid.html">Make bid</a>
                            </td>
                            <td>
                                <a href="info_picture.html">More info</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="bid.html">Make bid</a>
                            </td>
                            <td>
                                <a href="info_vase.html">More info</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="bid.html">Make bid</a>
                            </td>
                            <td>
                                <a href="info_picture2.html">More info</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="bid.html">Make bid</a>
                            </td>
                            <td>
                                <a href="info_fresco.html">More info</a>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>
