package com.test.web.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Create class LogoutServlet implementing interface HttpServlet
 * class is used to exit out of their credentials in the online auction
 * Created by ksu on 07.10.2016.
 */
public class LogoutServlet extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out=resp.getWriter();

        //Displays links to other pages
        req.getRequestDispatcher("link.html").include(req, resp);

        //End session
        HttpSession session=req.getSession();
        session.invalidate();

        out.print("<br><h3 align=\"center\" style=\"color: black\">Good Bye!</h3><br>");
        out.close();
    }
}
